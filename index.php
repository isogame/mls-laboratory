<HTML>
    <HEAD>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <TITLE>MLS研究所</TITLE></HEAD>
    <BODY>
        <H2>MLS研究所</H2><H3>MLSのデータロストが起こりにくい時間の解明をするプロジェクト</H3>
        
        <?php
        require_once("database_connector.php");
        $db_session = dbconnect();

        $sqlquery = "SELECT * from status order by SendTime desc LIMIT 0,1000";
        unset($db_result);
        if ($result = $db_session->query($sqlquery)) {
            while ($row_data = $result->fetch_assoc()) {
                $db_result[] = $row_data;
            }
            $result->free();
        }
        
        echo '<style type="text/css"><!--';
        echo 'div.playerprofile_playmusictop10_tableall { display: table; table-layout: fixed; font-size: 14px; width: 100%; max-width: 400px; text-align: center; word-wrap: break-word; border-width: 0px 0px 1px 0px; border-color: #2d6495; border-style: solid; height: 30px; }';
        echo 'div.playerprofile_playmusictop10_cellmname { display: table-cell; vertical-align: middle; width: 50%; border-width: 0px 1px 0px 1px; border-color: #2d6495; border-style: solid; }';
        echo 'div.playerprofile_playmusictop10_cellcount { display: table-cell; vertical-align: middle; width: 50%; border-width: 0px 1px 0px 0px; border-color: #2d6495; border-style: solid; position: relative; padding-top: 4px; padding-bottom: 9px; }';
        echo '--></style>';
        echo '<div style="height: auto;" class="playerprofile_playmusictop10_tableall">';
            echo '<div class="playerprofile_playmusictop10_cellmname" style="background-color: lightgray; border-width: 1px 1px 0px 1px;">MLSに送った時間</div>';
            echo '<div class="playerprofile_playmusictop10_cellcount" style="background-color: lightgray; border-width: 1px 1px 0px 0px;">MLSに反映された時間</div>';
        echo '</div>';
        
        for($rec=0; $rec < count($db_result); $rec++) {
            if ($db_result[$rec]['UpdateTime'] == '0000-00-00 00:00:00') {
                $UpdateDelay="未反映";
                $UpdateTime="";
            } else {
                $UpdateDelay=floor((strtotime($db_result[$rec]['UpdateTime'])-strtotime($db_result[$rec]['SendTime']))/60);
                if ($UpdateDelay > 60) {
                    $UpdateDelay=floor($UpdateDelay/60).'時間';
                } else {
                    $UpdateDelay=$UpdateDelay.'分';
                }
                $UpdateTime=$db_result[$rec]['UpdateTime'];
            } 
            echo '<div style="height: auto;" class="playerprofile_playmusictop10_tableall">';
            echo '<div class="playerprofile_playmusictop10_cellmname">'.$db_result[$rec]['SendTime'].'</div>';
            echo '<div class="playerprofile_playmusictop10_cellcount"><span style="font-size: 21px;">'.$UpdateDelay.'</span><BR>'.$UpdateTime.'</div>';
            echo '</div>';
        }
        
        //MySQL接続解除
        dbdisconnect($db_session);
        ?>
    </BODY>
</HTML>