<?php
date_default_timezone_set('Asia/Tokyo');

//Debug
//$forcelocalfile = "true";
$forcelocalfile = "false";
//$forcefilename = "MLS-diff-cell-export-2023-11-20T230000.csv.gz";

if ($forcelocalfile !== "true") {
    $rawfilename = "MLS-diff-cell-export-".gmdate('Y-m-d')."T".gmdate('H')."0000.csv.gz";
    echo "Downloading ".$rawfilename."...".PHP_EOL;
    $header = stream_context_create([
        'http' => ['header' => 'Accept-Encoding: gzip'],
    ]);
    $gzipcontents = file_get_contents('https://d2koia3g127518.cloudfront.net/export/'.$rawfilename,False,$header);
}

if ($forcelocalfile == "true") {
    echo "DEBUG MODE: Continue Local File: ./Raw/".$forcefilename.PHP_EOL;
    $csvcontents = file_get_contents('compress.zlib://'.__DIR__.'/Raw/'.$forcefilename);
} elseif ($http_response_header[0] == "HTTP/1.1 200 OK") {
    echo "200 OK Download Completed".PHP_EOL;
    file_put_contents(__DIR__.'/Raw/'.$rawfilename,$gzipcontents);
    $csvcontents = file_get_contents('compress.zlib://'.__DIR__.'/Raw/'.$rawfilename);
} elseif ($http_response_header[0] == "HTTP/1.1 404 Not Found") {
    echo "404 Not Found; Download Error, Abort.".PHP_EOL;
    die();
} else {
    echo "Unknown HTTP Error; Download Error, Abort.".PHP_EOL;
    die();
}

//echo $csvcontents;

$alllines = substr_count($csvcontents, "\n");
echo "LINE Count: ".$alllines.PHP_EOL;

$japanlines = preg_match_all("/^....?,44[0,1],/ms", $csvcontents, $matches);
echo "440/441 LINE Count: ".$japanlines.PHP_EOL;
//var_dump($matches);

$rktnlines = preg_match_all("/^LTE,440,11/ms", $csvcontents, $matches);
echo "44011 LINE Count: ".$rktnlines.PHP_EOL;
//var_dump($matches);

//Send
$sendfront = array(
    'mode' => 'create',
    'alllines' => $alllines,
    'japanlines' => $japanlines,
    'rktnlines' => $rktnlines
);
$sendfront=http_build_query($sendfront,"","&");

$context = array(
    'http' => array(
        'method' => 'POST',
        'header' => implode("\r\n", array(
            'Content-Type: application/x-www-form-urlencoded',
            'Content-Length: '.strlen($sendfront))),
        'content' => $sendfront
    )
);
$response = file_get_contents('https://cbs.brave-hero.net/44011/mls-result/modify.php', false, stream_context_create($context));
echo "response: ".$response.PHP_EOL;
$response = '';
$sendfront = '';

$mlsdata=array('94914584', #eNB370760-24
    '94914561', #eNB370760-1
    '94914562', #eNB370760-2
    '94914563', #eNB370760-3
    '94914564', #eNB370760-4
    '94914565', #eNB370760-5
    '94914566', #eNB370760-6
    '94914823', #eNB370761-7
    '94914568', #eNB370760-8
    '94914569', #eNB370760-9
    '94914826', #eNB370761-10
    '94914571', #eNB370760-11
    '94914572', #eNB370760-12
    '94914829', #eNB370761-13
    '94914830', #eNB370761-14
    '94914575', #eNB370760-15
    '94914832', #eNB370761-16
    '94914577', #eNB370760-17
    '94914578', #eNB370760-18
    '94914579', #eNB370760-19
    '94914836', #eNB370761-20
    '94914581', #eNB370760-21
    '94914582', #eNB370760-22
    '94914583'); #eNB370760-23

//xx:15:00 Send MLS Data Scan
$processingtime = date("H");
for ($processingtime=0; $processingtime <= 23; $processingtime++) {
    echo "Analyzing Send Time ".$processingtime."...".PHP_EOL;
    $matchresult = preg_match("/^LTE,440,11,.*?,(".$mlsdata[$processingtime]."),.*?,.*?,.*?,.*?,.*?,.*?,.*?,(.*?),/ms", $csvcontents, $matches);
    if (date('H') < $processingtime) {
        $sendtime = date("Y-m-d ".$processingtime.":15:00", strtotime('-1day'));
    } else {
        $sendtime = date("Y-m-d ".$processingtime.":15:00");
    }
    if ($matchresult > 0) {
        echo "Match!: ".$matchresult.PHP_EOL;
        $updatetime = (new DateTime('Asia/Tokyo'))->setTimestamp($matches[2])->format("Y/m/d H:i:s");
        
        //Send
        $sendfront = array(
            'mode' => 'update',
            'sendtime' => $sendtime,
            'updatetime' => $updatetime
        );
        $sendfront=http_build_query($sendfront,"","&");
        $context = array(
            'http' => array(
            'method' => 'POST',
            'header' => implode("\r\n", array(
                        'Content-Type: application/x-www-form-urlencoded',
                        'Content-Length: '.strlen($sendfront))),
            'content' => $sendfront
        )
    );
    $response = file_get_contents('https://server-address/path-to-project/Modify.php', false, stream_context_create($context));
    echo "...Data Upload Complete.".PHP_EOL;
    } else {
        echo "Not Updated.".PHP_EOL;
    }
    
}
?>