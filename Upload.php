<?php
$url = "https://location.services.mozilla.com/v2/geosubmit";

switch ( date('H') ){
    case 0:
        #eNB370760-24
        $data = '{"items":[{"timestamp":1692612677671,"position":{"latitude":35.094502611391,"longitude":136.999119767953,"accuracy":5.84,"altitude":42.73,"heading":0,"speed":0.14,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914584,"primaryScramblingCode":429,"asu":31,"signalStrength":-51,"serving":1}]}]}';
        break;
    case 1:
        #eNB370760-1
        $data = '{"items":[{"timestamp":1692614384722,"position":{"latitude":35.094471779235,"longitude":136.998998914688,"accuracy":5.74,"altitude":45.38,"heading":0,"speed":0.46,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914561,"primaryScramblingCode":467,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 2:
        #eNB370760-2
        $data = '{"items":[{"timestamp":1692613063506,"position":{"latitude":35.094524701412,"longitude":136.99910229957,"accuracy":5.11,"altitude":44.74,"heading":0,"speed":0.51,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914562,"primaryScramblingCode":415,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 3:
        #eNB370760-3
        $data = '{"items":[{"timestamp":1692612972982,"position":{"latitude":35.094522972896,"longitude":136.999100336236,"accuracy":2.65,"altitude":42.52,"heading":0,"speed":0.47,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914563,"primaryScramblingCode":407,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 4:
        #eNB370760-4
        $data = '{"items":[{"timestamp":1692612163133,"position":{"latitude":35.09451704854,"longitude":136.99910636811,"accuracy":2.48,"altitude":44.06,"heading":0,"speed":0.14,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914564,"primaryScramblingCode":409,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 5:
        #eNB370760-5
        $data = '{"items":[{"timestamp":1692612376950,"position":{"latitude":35.094487651025,"longitude":136.999132660964,"accuracy":2.17,"altitude":43.94,"heading":0,"speed":0.64,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914565,"primaryScramblingCode":420,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 6:
        #eNB370760-6
        $data = '{"items":[{"timestamp":1692616696996,"position":{"latitude":35.094041710809,"longitude":136.99988801178,"accuracy":1.16,"altitude":45.74,"heading":0,"speed":0.48,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914566,"primaryScramblingCode":409,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 7:
        #eNB370761-7
        $data = '{"items":[{"timestamp":1692614043395,"position":{"latitude":35.094495746554,"longitude":136.999037241379,"accuracy":4.72,"altitude":46.63,"heading":0,"speed":0.26,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914823,"primaryScramblingCode":472,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 8:
        #eNB370760-8
        $data = '{"items":[{"timestamp":1692616008034,"position":{"latitude":35.094075789465,"longitude":136.999895466297,"accuracy":2.61,"altitude":46.55,"heading":0,"speed":0.06,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914568,"primaryScramblingCode":436,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 9:
        #eNB370760-9
        $data = '{"items":[{"timestamp":1692613479504,"position":{"latitude":35.094508856967,"longitude":136.999052672242,"accuracy":5.28,"altitude":46.27,"heading":0,"speed":0.25,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914569,"primaryScramblingCode":466,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 10:
        #eNB370761-10
        $data = '{"items":[{"timestamp":1692612635580,"position":{"latitude":35.094515503323,"longitude":136.999122036855,"accuracy":3.2,"altitude":45.8,"heading":0,"speed":0.34,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914826,"primaryScramblingCode":430,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 11:
        #eNB370760-11
        $data = '{"items":[{"timestamp":1692613943859,"position":{"latitude":35.094502446357,"longitude":136.999030164941,"accuracy":3.53,"altitude":42.59,"heading":0,"speed":0.25,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914571,"primaryScramblingCode":470,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 12:
        #eNB370760-12
        $data = '{"items":[{"timestamp":1692614526331,"position":{"latitude":35.094458436519,"longitude":136.998997468454,"accuracy":1.94,"altitude":42.5,"heading":0,"speed":0.15,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914572,"primaryScramblingCode":473,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 13:
        #eNB370761-13
        $data = '{"items":[{"timestamp":1692616128367,"position":{"latitude":35.094056548465,"longitude":136.999888041304,"accuracy":1.45,"altitude":42.46,"heading":0,"speed":0.3,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914829,"primaryScramblingCode":423,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 14:
        #eNB370761-14
        $data = '{"items":[{"timestamp":1692614142904,"position":{"latitude":35.09449218285,"longitude":136.999029697729,"accuracy":3.85,"altitude":45.03,"heading":0,"speed":0.4,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914830,"primaryScramblingCode":443,"asu":31,"signalStrength":-51,"serving":1}]}]}';
        break;
    case 15:
        #eNB370760-15
        $data = '{"items":[{"timestamp":1692611883251,"position":{"latitude":35.094485766106,"longitude":136.999112774203,"accuracy":4.04,"altitude":44.16,"heading":0,"speed":0.35,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914575,"primaryScramblingCode":413,"asu":27,"signalStrength":-59,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 16:
        #eNB370761-16
        $data = '{"items":[{"timestamp":1692612319773,"position":{"latitude":35.094488142287,"longitude":136.999120873258,"accuracy":2.32,"altitude":44.33,"heading":0,"speed":0.68,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914832,"primaryScramblingCode":428,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 17:
        #eNB370760-17
        $data = '{"items":[{"timestamp":1692616471251,"position":{"latitude":35.094049653265,"longitude":136.999892778197,"accuracy":2.59,"altitude":43.45,"heading":0,"speed":0,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914577,"primaryScramblingCode":405,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 18:
        #eNB370760-18
        $data = '{"items":[{"timestamp":1692616338965,"position":{"latitude":35.094078072215,"longitude":136.999907246742,"accuracy":3.48,"altitude":42.28,"heading":0,"speed":0.13,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914578,"primaryScramblingCode":412,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 19:
        #eNB370760-19
        $data = '{"items":[{"timestamp":1692615881746,"position":{"latitude":35.094078486971,"longitude":136.999888641795,"accuracy":2.42,"altitude":45.87,"heading":0,"speed":0.52,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914579,"primaryScramblingCode":429,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 20:
        #eNB370761-20
        $data = '{"items":[{"timestamp":1692613901474,"position":{"latitude":35.094496970812,"longitude":136.999030184399,"accuracy":5.67,"altitude":46.8,"heading":0,"speed":0.18,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914836,"primaryScramblingCode":469,"asu":31,"signalStrength":-51,"serving":1}]}]}';
        break;
    case 21:
        #eNB370760-21
        $data = '{"items":[{"timestamp":1692613738730,"position":{"latitude":35.094508381987,"longitude":136.999056267421,"accuracy":1.45,"altitude":42.08,"heading":0,"speed":0.22,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914581,"primaryScramblingCode":452,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 22:
        #eNB370760-22
        $data = '{"items":[{"timestamp":1692614677109,"position":{"latitude":35.0944553989,"longitude":136.998989362069,"accuracy":3.03,"altitude":42.95,"heading":0,"speed":0.64,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914582,"primaryScramblingCode":449,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    case 23:
        #eNB370760-23
        $data = '{"items":[{"timestamp":1692613310708,"position":{"latitude":35.094524623361,"longitude":136.999080455931,"accuracy":3.33,"altitude":45.32,"heading":0,"speed":0.52,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":94914583,"primaryScramblingCode":418,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
        break;
    default:
        #eNB375467-9
        $data = '{"items":[{"timestamp":1692012976824,"position":{"latitude":35.16989267,"longitude":136.87838689,"accuracy":10.26,"altitude":71.48,"heading":0,"speed":0,"source":"gps"},"cellTowers":[{"radioType":"lte","mobileCountryCode":440,"mobileNetworkCode":11,"locationAreaCode":33411,"cellId":96119561,"primaryScramblingCode":394,"asu":31,"signalStrength":-51,"timingAdvance":0,"serving":1}]}]}';
 }

$context = array(
    'http' => array(
        'method' => 'POST',
        'header' => implode("\r\n", array(
                    'Content-Type: application/json; charset=utf8',
                    'User-Agent: MLSLaboratory/1.0',
                    'Content-Length: '.strlen($data))),
        'content' => $data
    )
);

$response = file_get_contents($url, false, stream_context_create($context));

echo $response;
?>