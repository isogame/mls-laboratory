<?php
//MySQL接続
require_once("database_connector.php");
$db_session = dbconnect();

$mode=escs($db_session,$_POST["mode"]);
$alllines=escs($db_session,$_POST["alllines"]);
$japanlines=escs($db_session,$_POST["japanlines"]);
$rktnlines=escs($db_session,$_POST["rktnlines"]);
$sendtime=escs($db_session,$_POST["sendtime"]);
$updatetime=escs($db_session,$_POST["updatetime"]);

echo "Modify Started.".PHP_EOL;
echo "alllines:".$alllines.PHP_EOL;
echo "japanlines:".$japanlines.PHP_EOL;
echo "rktnlines:".$rktnlines.PHP_EOL;
echo "sendtime:".$sendtime.PHP_EOL;
echo "updatetime:".$updatetime.PHP_EOL;
echo PHP_EOL;

if ($mode == "create") {
    echo "MODE: create".PHP_EOL;
    //時間ごとのMLS行数を書く
    $sqlquery = "INSERT INTO outline (dt,total,jp,rktn) VALUES ('".date("Y-m-d H:00:00")."','".$alllines."','".$japanlines."','".$rktnlines."')";
    if (!$result = $db_session->query($sqlquery)) { echo "error"; }
    echo "outline: new data created.".PHP_EOL;
    
    //次の呼び出し時に書かれるデータを作成（e.x. 13時50分の呼び出しで13時のデータを作り、14時50分の呼び出しで↓↓のUPDATEで実際のデータを書く）
    $sqlquery = "INSERT INTO status (SendTime,UpdateTime) VALUES ('".date("Y-m-d H:15:00")."','')";
    if (!$result = $db_session->query($sqlquery)) { echo "error"; }
    echo "status: new data created.".PHP_EOL;
} elseif ($mode == "update") {
    echo "MODE: update".PHP_EOL;
    //前時にcreateしたときのデータが存在しているはずなのでUPDATEできるはず
    $sqlquery = "UPDATE status SET UpdateTime = '".$updatetime."' WHERE sendtime='".$sendtime."'";
    if (!$result = $db_session->query($sqlquery)) { echo "error"; }
    echo "status: ".$sendtime." data updated.".PHP_EOL;
} else {
    echo "MODE: Unknown. Abort".PHP_EOL;
}

//MySQL接続解除
dbdisconnect($db_session);
?>